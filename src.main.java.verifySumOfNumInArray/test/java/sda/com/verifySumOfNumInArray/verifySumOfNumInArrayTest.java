package sda.com.verifySumOfNumInArray;

import static org.junit.Assert.assertTrue;

import org.junit.Assert;
import org.junit.Test;
import sda.com.verifySumOfNumInArray.resources.VerifySumOfNumbers;

/**
 * Unit test for simple App.
 */
public class verifySumOfNumInArrayTest
{
    /**
     * Rigorous Test :-)
     */
    //testing the first method that verifies if two numbers in array equal given number
    @Test
    public void verifySumOfNumTest1() {
        VerifySumOfNumbers verifySumOfNumbers = new VerifySumOfNumbers();
        boolean myResult = verifySumOfNumbers.verifyIfSumOfTwoNumbersEqualMyNumber();

        boolean myTest = true;
        Assert.assertEquals(myTest, myResult );
    }

    //testing the second method that verifies if two numbers in array equal given number
    @Test
    public void verifySumOfNumTest2() {
        VerifySumOfNumbers verifySumOfNumbers = new VerifySumOfNumbers();
        boolean myResult = verifySumOfNumbers.verifyIfSumEqualsMyNumber();

        boolean myTest = true;
        Assert.assertEquals(myTest, myResult );
    }
}
