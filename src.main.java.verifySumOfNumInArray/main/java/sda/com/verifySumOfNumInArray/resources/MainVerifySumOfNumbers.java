package sda.com.verifySumOfNumInArray.resources;

import sda.com.verifySumOfNumInArray.resources.VerifySumOfNumbers;

import java.util.Arrays;
import java.util.Scanner;

public class MainVerifySumOfNumbers {
    public static void main(String[] args) {

        VerifySumOfNumbers verifySumOfNumbers = new VerifySumOfNumbers();
        //verifySumOfNumbers.verifyIfSumOfTwoNumbersEqualMyNumber();
        verifySumOfNumbers.verifyIfSumEqualsMyNumber();
    }
}

