package sda.com.verifySumOfNumInArray.resources;

import java.util.Arrays;


public class VerifySumOfNumbers {
    private int[] myArray;
    private int myNumber;

    // sorting ascending array
    public int[] makeAscendingArray() {


        int lengthOfArray = 5;
        System.out.println("lungimea arrayului este " + lengthOfArray);
        System.out.println("introduceti " + lengthOfArray + " numere");

        int[] myArray = {1, 5, 2, 4, 3};
        System.out.println(Arrays.toString(myArray));

        int[] myAscendingArray;
        Arrays.sort(myArray);
        myAscendingArray = myArray;
        System.out.println(Arrays.toString(myAscendingArray));
        return myAscendingArray;
    }

    //first method that checks if two numbers in my array equal a given number
    //it uses two loopes
    public boolean verifyIfSumOfTwoNumbersEqualMyNumber() {

        boolean verifySumOfNum = false;
        myNumber = 5;
        System.out.println("introduceti numarul pe care il verificam este " + myNumber);
        VerifySumOfNumbers verifySumOfNumbers = new VerifySumOfNumbers();
        int[] myAscendingArray = verifySumOfNumbers.makeAscendingArray();
        for (int i = 0; i < myAscendingArray.length; i++) {
            for (int j = i + 1; j < myAscendingArray.length; j++) {
                if (myAscendingArray[i] + myAscendingArray[j] == myNumber) {
                    verifySumOfNum = true;
                    System.out.println(myAscendingArray[i] + "+" + myAscendingArray[j] + "=" + myNumber);
                }
            }
        }

        return verifySumOfNum;
    }

    // second method that checks if two numbers in my array equal a given number
    //it uses one loop, adding the first and last elem of the array
    //then moving up/down inside the array
    public boolean verifyIfSumEqualsMyNumber() {
        boolean verifySumOfNum1 = false;

        myNumber = 5;
        System.out.println("numarul pe care il verificam este " + myNumber);
        VerifySumOfNumbers verifySumOfNumbers = new VerifySumOfNumbers();
        int[] myAscendingArray = verifySumOfNumbers.makeAscendingArray();
        int myLength = myAscendingArray.length - 1;
        int i = 0;
        int sum;
        while (i < myLength) {
            sum = myAscendingArray[i] + myAscendingArray[myLength];
            if (sum == myNumber) {
                verifySumOfNum1 = true;
                System.out.println(myAscendingArray[i] + "+" + myAscendingArray[myLength] + "=" + myNumber);
                i++;
                myLength--;
            } else if (sum > myNumber) {
                myLength--;
            } else if (sum < myNumber) {
                i++;
            }
        }
        return verifySumOfNum1;
    }
}

